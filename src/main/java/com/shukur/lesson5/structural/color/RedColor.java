package com.shukur.lesson5.structural.color;

public class RedColor implements Color{

    @Override
    public void fillColor() {
        System.out.println("Painting to the red color");
    }
}