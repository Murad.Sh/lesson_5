package com.shukur.lesson5.structural.color;

public class WhiteColor implements Color{

    @Override
    public void fillColor() {
        System.out.println("Painting to the white color");
    }
}