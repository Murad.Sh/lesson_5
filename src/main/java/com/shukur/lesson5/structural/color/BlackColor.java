package com.shukur.lesson5.structural.color;

public class BlackColor implements Color{

    @Override
    public void fillColor() {
        System.out.println("Painting to the black color");
    }
}
