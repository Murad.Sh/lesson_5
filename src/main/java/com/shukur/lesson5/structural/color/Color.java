package com.shukur.lesson5.structural.color;

public interface Color {

    void fillColor();
}
