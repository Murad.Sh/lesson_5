package com.shukur.lesson5.structural;

import com.shukur.lesson5.structural.car.SeriesFive;
import com.shukur.lesson5.structural.color.BlackColor;

public class BridgeMain {
    public static void main(String[] args) {
        SeriesFive s5 = new SeriesFive(new BlackColor());
        s5.produce();
    }
}
