package com.shukur.lesson5.structural.car;

import com.shukur.lesson5.structural.color.Color;

public class SeriesSeven extends Car{

    public SeriesSeven(Color color) {
        super(color);
    }

    @Override
    public void produce() {
        System.out.println("Producing new car series seven");
        color.fillColor();
    }
}
