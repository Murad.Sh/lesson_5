package com.shukur.lesson5.structural.car;

import com.shukur.lesson5.structural.color.Color;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public abstract class Car {
    public Color color;
    public abstract void produce();
}
