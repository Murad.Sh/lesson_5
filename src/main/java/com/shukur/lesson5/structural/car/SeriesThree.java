package com.shukur.lesson5.structural.car;

import com.shukur.lesson5.structural.color.Color;

public class SeriesThree extends Car{

    public SeriesThree(Color color) {
        super(color);
    }

    @Override
    public void produce() {
        System.out.println("Producing new car series three");
        color.fillColor();
    }
}
