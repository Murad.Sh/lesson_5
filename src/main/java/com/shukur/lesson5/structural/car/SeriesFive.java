package com.shukur.lesson5.structural.car;

import com.shukur.lesson5.structural.color.Color;

public class SeriesFive extends Car{

    public SeriesFive(Color color) {
        super(color);
    }

    @Override
    public void produce() {
        System.out.println("Producing new car series five");
        color.fillColor();
    }
}
