package com.shukur.lesson5.creational.builder;

public class BuilderMain {
    public static void main(String[] args) {
        Cake cake = new Cake.Builder()
                .firstIngredient("flour")
                .secondIngredient("sugar")
                .fifthIngredient("eggs")
                .build();

        System.out.println(cake.toString());
    }
}
