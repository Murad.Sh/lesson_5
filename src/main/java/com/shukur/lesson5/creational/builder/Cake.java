package com.shukur.lesson5.creational.builder;

import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Cake {
    String firstIngredient;
    String secondIngredient;
    String thirdIngredient;
    String fourthIngredient;
    String fifthIngredient;

    public static class Builder {
        private final Cake cake;

        public Builder() {
            this.cake = new Cake();
        }

        public Builder firstIngredient(String firstIngredient) {
            this.cake.firstIngredient = firstIngredient;
            return this;
        }

        public Builder secondIngredient(String firstIngredient) {
            this.cake.firstIngredient = firstIngredient;
            return this;
        }

        public Builder thirdIngredient(String firstIngredient) {
            this.cake.firstIngredient = firstIngredient;
            return this;
        }

        public Builder fourthIngredient(String firstIngredient) {
            this.cake.firstIngredient = firstIngredient;
            return this;
        }

        public Builder fifthIngredient(String firstIngredient) {
            this.cake.firstIngredient = firstIngredient;
            return this;
        }

        public Cake build() {
            return this.cake;
        }
    }
}
