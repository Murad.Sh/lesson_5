package com.shukur.lesson5.behavioural;

public interface HealthMediator {

    void createAccount(Client client);

    void makeAnAppointment(Client client);
}
