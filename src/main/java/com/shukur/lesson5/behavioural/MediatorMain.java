package com.shukur.lesson5.behavioural;

public class MediatorMain {
    public static void main(String[] args) {
        Hospital hospital = new Hospital();
        Laboratory lab = new Laboratory();

        Client client1 = new Client("Murad", "Shukurzadeh", hospital);
        Client client2 = new Client("Fikret", "Javadov", lab);
        Client client3 = new Client("Nuri", "Akhundov", hospital);

        client1.createAccount();
        client1.makeAnAppointment();

        client2.createAccount();
        client2.makeAnAppointment();

        client3.createAccount();
        client3.makeAnAppointment();
    }
}
