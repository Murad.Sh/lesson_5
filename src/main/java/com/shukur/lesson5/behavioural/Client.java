package com.shukur.lesson5.behavioural;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.experimental.FieldDefaults;

@Data
@AllArgsConstructor
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Client {
    String name;
    String surname;
    HealthMediator health;

    public void createAccount() {
        health.createAccount(this);
    }

    public void makeAnAppointment() {
        health.makeAnAppointment(this);
    }
}
