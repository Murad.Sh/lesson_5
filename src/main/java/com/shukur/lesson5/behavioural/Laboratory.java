package com.shukur.lesson5.behavioural;

public class Laboratory implements HealthMediator {

    @Override
    public void createAccount(Client client) {
        System.out.println(client.getSurname() + " " + client.getName() + " want to create an account for health check up in laboratory.");
    }

    public void makeAnAppointment(Client client) {
        System.out.println(client.getSurname() + " " + client.getName() + " want make an appointment for health check up in laboratory.");
    }
}
